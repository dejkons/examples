var config = require('./config');
var request = require('request');
var q = require('q');
var logger = require('logger').createLogger('development.log');
require('paint-console');

var sessionID = '';
var deviceId = '58d8d73bd90640ee10feaab1';
var userId = '58d1036e686d10671d8db509';

var jsonSoftwareObj = {"softwareID":"58d53d8c0aa1483b5d70fc3d","confirmation":"none"};
var globalCounter = 0;
var emptyAttemptsCounter = 0;
var interval = null;

function StartFirmwareUpgrade() {

        var loginUser = function () {
            var deferred = q.defer();
            request.post({
                    url: config.adminCredentials.apiUrl + 'auth/login',
                    form: {
                        identity: config.adminCredentials.identity,
                        secret: config.adminCredentials.secret
                    }
                },
                function (err, httpResponse, body) {
                    if (err) {
                        deferred.reject(new Error("Can't authenticate unfortunately!!!"));
                    } else {
                        deferred.resolve(httpResponse.headers.sessionid);
                    }
                });
            return deferred.promise;
        }

        var initiateCampaign = function () {
            logger.info("SessionID: " + sessionID);
            logger.info("Initiating campaign");
            var deferred = q.defer();
            request.post({
                    url: config.adminCredentials.apiUrl + 'udm/admin/campaign/initiate',
                    json: true,
                    body: jsonSoftwareObj,
                    headers: {'sessionid': sessionID}
                },
                function (error, response, body) {
                    if (error) {
                        deferred.reject(new Error("Can't initiate campaign unfortunately!!!"));
                    } else {
                        deferred.resolve(body);
                    }
                });
            return deferred.promise;
        }

        var errorCampaign = function (error) {
            clearInterval(interval);
            throw error;
            process.exit();
        }

        var getGatewayInfo = function () {
            logger.info("Checking current GW parameters...");
            var deferred = q.defer();
            request.get({
                    url: config.adminCredentials.apiUrl + 'udm/device/hg/' + deviceId + '/info',
                    json: true,
                    body: {'userId': userId},
                    headers: {'sessionid': sessionID}
                },
                function (error, response, body) {
                    if (error) {
                        deferred.reject(new Error("Can't resolve current GW parameters"));
                    } else {
                        deferred.resolve(body);
                    }
                });
            return deferred.promise;
        }

        var calculateFutureGWVersion = function (currentFWVersion) {

            var lastChar = currentFWVersion.charAt(currentFWVersion.length - 1);
            lastChar = Number(lastChar);

            var aboutToInstallSoftwareVersion = "3.0.0-TST6";

            switch (lastChar) {
                case 6:
                    aboutToInstallSoftwareVersion = "3.0.0-TST7";
                    break;
                case 7:
                    aboutToInstallSoftwareVersion = "3.0.0-TST6";
                    break;
            }

            return aboutToInstallSoftwareVersion;
        }


        var prepareCampaignObject = function (curentGWVersion, nextGWVersion) {
            jsonSoftwareObj = {
                "softwareID": "58d53d8c0aa1483b5d70fc3d",
                "confirmation": "none"
            };
        }


        /***********************************************************************************************/


        loginUser().then(
            function (sessId) {
                logger.info("User logged in succesfully");
                sessionID = sessId;
                getGatewayInfo().then(
                    function (gwInfo) {
                        if (gwInfo.installationInProgress == false) {
                            if (typeof gwInfo.currentFWVersion == 'undefined') {
                                process.exit();
                            }
                            logger.info("GW current version is " + gwInfo.currentFWVersion);
                            var upgradeFWVersion = calculateFutureGWVersion(gwInfo.currentFWVersion);
                            logger.info("Next GW version is " + upgradeFWVersion);
                            prepareCampaignObject(gwInfo.currentFWVersion, upgradeFWVersion);
                            initiateCampaign().then(function (res) {
                                globalCounter++;
                                //console.log(res);
                                logger.info("Number of install attempts: " + globalCounter);
                            }, errorCampaign);
                        } else {
                            logger.warn("Installation is in progress or GW paramters can't be resolved");
                            emptyAttemptsCounter++;
                            if (emptyAttemptsCounter == 5) {
                                logger.error("Exiting process because something is wrong with GW");
                                logger.info("Number of success attempts " + globalCounter);
                                process.exit();
                            }
                        }
                    }, function (err) {
                        clearInterval(interval);
                        throw err;
                        process.exit();
                    }
                )
            },
            function (err) {
                clearInterval(interval);
                throw err;
                process.exit();
            }
        );

}

StartFirmwareUpgrade();

interval = setInterval(StartFirmwareUpgrade, 300000);