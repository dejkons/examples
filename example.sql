USE [test]
GO
/****** Object:  StoredProcedure [dbo].[NineTestsProcedureCore]    Script Date: 09/13/2016 13:35:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dejan Adamovic
-- Create date: 09.09.2016.
-- Description:	Stored procedure built for BIRT report
-- =============================================

IF OBJECT_ID ('dbo.NineTestsProcedureCore','p') IS NULL 
exec('CREATE PROCEDURE dbo.NineTestsProcedureCore as select 1')
GO

ALTER PROCEDURE [dbo].[NineTestsProcedureCore] @DateFrom nvarchar(100), @DateTo nvarchar(100)
AS
BEGIN

-- Defining split function

exec('IF OBJECT_ID (N''SplitDelimiteredStrings'', N''FN'') IS NOT NULL 
DROP FUNCTION SplitDelimiteredStrings;')

exec('CREATE FUNCTION dbo.SplitDelimiteredStrings(@str   VARCHAR(5000), @POSITION INT)
returns VARCHAR(5000)
AS
  BEGIN
      DECLARE @TEMP   VARCHAR(2),
              @CNTR   INT =1,
              @RESULT VARCHAR(5000),
              @INTR   INT =0,
              @LEN    INT

      SET @LEN =Len(@str)
      WHILE @CNTR <= @LEN
        BEGIN
            SET @TEMP = Substring(@STR, @CNTR, 1)
            IF @TEMP = '',''
              BEGIN
                  SET @INTR += 1
                  IF @INTR = @POSITION
                    BEGIN
                        SET @RESULT = Substring(@STR, @CNTR + 1, CASE
                                                                   WHEN Charindex('','', @STR, @CNTR + 1) - @CNTR < 1 THEN Len(@str)
                                                                   ELSE Charindex('','', @STR, @CNTR + 1) - @CNTR
                                                                 END - 1)

                        BREAK
                    END
              END
            SET @CNTR+=1
        END
      RETURN @RESULT
 END')

-- Defining view - use it for merging data form one of the tables

exec('IF OBJECT_ID (''tempMeasurementData'', ''V'') IS NOT NULL 
DROP VIEW tempMeasurementData;') 


exec('CREATE VIEW dbo.tempMeasurementData as SELECT TTR_ID,STUFF(
(select '','' + TMD_MEASUREMENT from dbo.TMEASUREMENTDATA where TTR_ID = a.TTR_ID for XML PATH ('''')),1,1,'''') as MERGED_TTR_VALUES 
from
dbo.TMEASUREMENTDATA as a															     															  
group by TTR_ID')

-- Create temp data where everything will be stored for further analysis

--IF OBJECT_ID ('tempdb..#tempNineTestsTable') IS NOT NULL 
--DROP TABLE #tempNineTestsTable

/*create table #tempNineTestsTable
(
    TEXP_ID Bigint,
    PCB_SN Varchar(100), 
    SN Varchar(100), 
    [SID] Varchar(100), 
    PRODUCTION_DATE Datetime,
    USER_CREATED Varchar(20),
    TEST1 Varchar(50),
    TEST2 Varchar(50),
    TEST3 Varchar(50),
    TEST4 Varchar(50),
    TEST5 Varchar(50),
    TEST6 Varchar(50),
    TEST7 Varchar(50),
    TEST8 Varchar(50),
    TEST9 Varchar(50)
)*/

--insert into #tempNineTestsTable
select
finalTableWithoutUsers.TEXP_ID,
finalTableWithoutUsers.TTR_ID,
finalTableWithoutUsers.PCB_SN,
finalTableWithoutUsers.SN,
finalTableWithoutUsers.[SID],
dbo.TEXECUTIONPARAMETERS.TEXP_START_DATETIME as PRODUCTION_DATE,
dbo.TUSER.TU_USERNAME as USER_CREATED,
finalTableWithoutUsers.TEST1,
finalTableWithoutUsers.TEST2,
finalTableWithoutUsers.TEST3,
finalTableWithoutUsers.TEST4,
finalTableWithoutUsers.TEST5,
finalTableWithoutUsers.TEST6,
finalTableWithoutUsers.TEST7,
finalTableWithoutUsers.TEST8,
finalTableWithoutUsers.TEST9
from 
(select
finalBugFixTable.TEXP_ID,
finalBugFixTable.TTR_ID,
(case
    when finalBugFixTable.TTR_VALUE = finalBugFixTable.TTR_VALUE2 and finalBugFixTable.TTR_VALUE2 = finalBugFixTable.TTR_VALUE3 and finalBugFixTable.TTR_VALUE3 = finalBugFixTable.TTR_VALUE4 and finalBugFixTable.TTR_VALUE4 = finalBugFixTable.TTR_VALUE5 and finalBugFixTable.TTR_VALUE5 = finalBugFixTable.TTR_VALUE6 and finalBugFixTable.TTR_VALUE6 = finalBugFixTable.TTR_VALUE7 and finalBugFixTable.TTR_VALUE7 = finalBugFixTable.TTR_VALUE8 then finalBugFixTable.TTR_VALUE
    when finalBugFixTable.TTR_VALUE is not null and LEN(finalBugFixTable.TTR_VALUE) < 16 then finalBugFixTable.TTR_VALUE
    when finalBugFixTable.TTR_VALUE2 is not null and LEN(finalBugFixTable.TTR_VALUE2) < 16 then finalBugFixTable.TTR_VALUE 
    when finalBugFixTable.TTR_VALUE3 is not null and LEN(finalBugFixTable.TTR_VALUE3) < 16 then finalBugFixTable.TTR_VALUE2 
    when finalBugFixTable.TTR_VALUE4 is not null and LEN(finalBugFixTable.TTR_VALUE4) < 16 then finalBugFixTable.TTR_VALUE3 
    when finalBugFixTable.TTR_VALUE5 is not null and LEN(finalBugFixTable.TTR_VALUE5) < 16 then finalBugFixTable.TTR_VALUE4 
    when finalBugFixTable.TTR_VALUE6 is not null and LEN(finalBugFixTable.TTR_VALUE6) < 16 then finalBugFixTable.TTR_VALUE5 
    when finalBugFixTable.TTR_VALUE7 is not null and LEN(finalBugFixTable.TTR_VALUE7) < 16 then finalBugFixTable.TTR_VALUE6 
    when finalBugFixTable.TTR_VALUE8 is not null and LEN(finalBugFixTable.TTR_VALUE8) < 16 then finalBugFixTable.TTR_VALUE7  
    else NULL
 end   
) as PCB_SN,
(case 
    when (select dbo.SplitDelimiteredStrings(finalBugFixTable.TTR_VALUE9, 1)) is not null and LEN((select dbo.SplitDelimiteredStrings(finalBugFixTable.TTR_VALUE9, 1))) = 12 and ((select dbo.SplitDelimiteredStrings(finalBugFixTable.TTR_VALUE9, 1))) != '' then (select dbo.SplitDelimiteredStrings(finalBugFixTable.TTR_VALUE9, 1))
    else NULL
 end   
) as SN,
(case 
    when (select dbo.SplitDelimiteredStrings(finalBugFixTable.TTR_VALUE9, 2)) is not null and LEN((select dbo.SplitDelimiteredStrings(finalBugFixTable.TTR_VALUE9, 2))) = 16 and ((select dbo.SplitDelimiteredStrings(finalBugFixTable.TTR_VALUE9, 2))) != '' then (select dbo.SplitDelimiteredStrings(finalBugFixTable.TTR_VALUE9, 2))
    else NULL
 end   
) as [SID],
finalBugFixTable.TEST1,
finalBugFixTable.TEST2,
finalBugFixTable.TEST3,
finalBugFixTable.TEST4,
finalBugFixTable.TEST5,
finalBugFixTable.TEST6,
finalBugFixTable.TEST7,
finalBugFixTable.TEST8,
finalBugFixTable.TEST9   
from 
	(select 
	finalTestTable.TEXP_ID,
	(case when finalTestTable.TESTNAME1 = 'PTP_DOC400_RDSN' then finalTestTable.TTR_ID else NULL end) as TTR_ID,
	(case 
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_RDSN' then NULL
		  when finalTestTable.TESTNAME1 = 'PTP_DOC400_QC' or finalTestTable.TESTNAME1 = 'PTP_DOC400_QC_WITHOUT_BURN' then finalTestTable.TTR_ID
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_QC' or finalTestTable.TESTNAME2 = 'PTP_DOC400_QC_WITHOUT_BURN' then finalTestTable.TTR_ID2
		  when finalTestTable.TESTNAME3 = 'PTP_DOC400_QC' or finalTestTable.TESTNAME3 = 'PTP_DOC400_QC_WITHOUT_BURN' then finalTestTable.TTR_ID3
		  when finalTestTable.TESTNAME4 = 'PTP_DOC400_QC' or finalTestTable.TESTNAME4 = 'PTP_DOC400_QC_WITHOUT_BURN' then finalTestTable.TTR_ID4
		  when finalTestTable.TESTNAME5 = 'PTP_DOC400_QC' or finalTestTable.TESTNAME5 = 'PTP_DOC400_QC_WITHOUT_BURN' then finalTestTable.TTR_ID5
		  when finalTestTable.TESTNAME6 = 'PTP_DOC400_QC' or finalTestTable.TESTNAME6 = 'PTP_DOC400_QC_WITHOUT_BURN' then finalTestTable.TTR_ID6
		  when finalTestTable.TESTNAME7 = 'PTP_DOC400_QC' or finalTestTable.TESTNAME7 = 'PTP_DOC400_QC_WITHOUT_BURN' then finalTestTable.TTR_ID7
		  when finalTestTable.TESTNAME8 = 'PTP_DOC400_QC' or finalTestTable.TESTNAME8 = 'PTP_DOC400_QC_WITHOUT_BURN' then finalTestTable.TTR_ID8
		  when finalTestTable.TESTNAME9 = 'PTP_DOC400_QC' or finalTestTable.TESTNAME9 = 'PTP_DOC400_QC_WITHOUT_BURN' then finalTestTable.TTR_ID9
	 else NULL end) as TTR_ID2,
	(case 
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME3 = 'PTP_DOC400_RDSN' then NULL
		  when finalTestTable.TESTNAME1 = 'PTP_DOC400_ZIGBEE' then finalTestTable.TTR_ID
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_ZIGBEE' then finalTestTable.TTR_ID2
		  when finalTestTable.TESTNAME3 = 'PTP_DOC400_ZIGBEE' then finalTestTable.TTR_ID3
		  when finalTestTable.TESTNAME4 = 'PTP_DOC400_ZIGBEE' then finalTestTable.TTR_ID4
		  when finalTestTable.TESTNAME5 = 'PTP_DOC400_ZIGBEE' then finalTestTable.TTR_ID5
		  when finalTestTable.TESTNAME6 = 'PTP_DOC400_ZIGBEE' then finalTestTable.TTR_ID6
		  when finalTestTable.TESTNAME7 = 'PTP_DOC400_ZIGBEE' then finalTestTable.TTR_ID7
		  when finalTestTable.TESTNAME8 = 'PTP_DOC400_ZIGBEE' then finalTestTable.TTR_ID8
		  when finalTestTable.TESTNAME9 = 'PTP_DOC400_ZIGBEE' then finalTestTable.TTR_ID9
	 else NULL end) as TTR_ID3,
	(case 
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME3 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME4 = 'PTP_DOC400_RDSN' then NULL
		  when finalTestTable.TESTNAME1 = 'PTP_DOC400_ZWAVE_PROG' then finalTestTable.TTR_ID
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_ZWAVE_PROG' then finalTestTable.TTR_ID2
		  when finalTestTable.TESTNAME3 = 'PTP_DOC400_ZWAVE_PROG' then finalTestTable.TTR_ID3
		  when finalTestTable.TESTNAME4 = 'PTP_DOC400_ZWAVE_PROG' then finalTestTable.TTR_ID4
		  when finalTestTable.TESTNAME5 = 'PTP_DOC400_ZWAVE_PROG' then finalTestTable.TTR_ID5
		  when finalTestTable.TESTNAME6 = 'PTP_DOC400_ZWAVE_PROG' then finalTestTable.TTR_ID6
		  when finalTestTable.TESTNAME7 = 'PTP_DOC400_ZWAVE_PROG' then finalTestTable.TTR_ID7
		  when finalTestTable.TESTNAME8 = 'PTP_DOC400_ZWAVE_PROG' then finalTestTable.TTR_ID8
		  when finalTestTable.TESTNAME9 = 'PTP_DOC400_ZWAVE_PROG' then finalTestTable.TTR_ID9
	 else NULL end) as TTR_ID4,
	(case 
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME3 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME4 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME5 = 'PTP_DOC400_RDSN' then NULL
		  when finalTestTable.TESTNAME1 = 'PTP_DOC400_ZWAVE_COMM' then finalTestTable.TTR_ID
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_ZWAVE_COMM' then finalTestTable.TTR_ID2
		  when finalTestTable.TESTNAME3 = 'PTP_DOC400_ZWAVE_COMM' then finalTestTable.TTR_ID3
		  when finalTestTable.TESTNAME4 = 'PTP_DOC400_ZWAVE_COMM' then finalTestTable.TTR_ID4
		  when finalTestTable.TESTNAME5 = 'PTP_DOC400_ZWAVE_COMM' then finalTestTable.TTR_ID5
		  when finalTestTable.TESTNAME6 = 'PTP_DOC400_ZWAVE_COMM' then finalTestTable.TTR_ID6
		  when finalTestTable.TESTNAME7 = 'PTP_DOC400_ZWAVE_COMM' then finalTestTable.TTR_ID7
		  when finalTestTable.TESTNAME8 = 'PTP_DOC400_ZWAVE_COMM' then finalTestTable.TTR_ID8
		  when finalTestTable.TESTNAME9 = 'PTP_DOC400_ZWAVE_COMM' then finalTestTable.TTR_ID9
	 else NULL end) as TTR_ID5,  
	(case 
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME3 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME4 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME5 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME6 = 'PTP_DOC400_RDSN' then NULL
		  when finalTestTable.TESTNAME1 = 'PTP_DOC400_WIFI' then finalTestTable.TTR_ID
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_WIFI' then finalTestTable.TTR_ID2
		  when finalTestTable.TESTNAME3 = 'PTP_DOC400_WIFI' then finalTestTable.TTR_ID3
		  when finalTestTable.TESTNAME4 = 'PTP_DOC400_WIFI' then finalTestTable.TTR_ID4
		  when finalTestTable.TESTNAME5 = 'PTP_DOC400_WIFI' then finalTestTable.TTR_ID5
		  when finalTestTable.TESTNAME6 = 'PTP_DOC400_WIFI' then finalTestTable.TTR_ID6
		  when finalTestTable.TESTNAME7 = 'PTP_DOC400_WIFI' then finalTestTable.TTR_ID7
		  when finalTestTable.TESTNAME8 = 'PTP_DOC400_WIFI' then finalTestTable.TTR_ID8
		  when finalTestTable.TESTNAME9 = 'PTP_DOC400_WIFI' then finalTestTable.TTR_ID9
	 else NULL end) as TTR_ID6, 
	(case 
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME3 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME4 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME5 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME6 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME7 = 'PTP_DOC400_RDSN' then NULL
		  when finalTestTable.TESTNAME1 = 'PTP_DOC400_FRONT_AND_LED' then finalTestTable.TTR_ID
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_FRONT_AND_LED' then finalTestTable.TTR_ID2
		  when finalTestTable.TESTNAME3 = 'PTP_DOC400_FRONT_AND_LED' then finalTestTable.TTR_ID3
		  when finalTestTable.TESTNAME4 = 'PTP_DOC400_FRONT_AND_LED' then finalTestTable.TTR_ID4
		  when finalTestTable.TESTNAME5 = 'PTP_DOC400_FRONT_AND_LED' then finalTestTable.TTR_ID5
		  when finalTestTable.TESTNAME6 = 'PTP_DOC400_FRONT_AND_LED' then finalTestTable.TTR_ID6
		  when finalTestTable.TESTNAME7 = 'PTP_DOC400_FRONT_AND_LED' then finalTestTable.TTR_ID7
		  when finalTestTable.TESTNAME8 = 'PTP_DOC400_FRONT_AND_LED' then finalTestTable.TTR_ID8
		  when finalTestTable.TESTNAME9 = 'PTP_DOC400_FRONT_AND_LED' then finalTestTable.TTR_ID9
	 else NULL end) as TTR_ID7,
	(case 
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME3 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME4 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME5 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME6 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME7 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME8 = 'PTP_DOC400_RDSN' then NULL
		  when finalTestTable.TESTNAME1 = 'PTP_DOC400_RST_AND_BUZZER' then finalTestTable.TTR_ID
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_RST_AND_BUZZER' then finalTestTable.TTR_ID2
		  when finalTestTable.TESTNAME3 = 'PTP_DOC400_RST_AND_BUZZER' then finalTestTable.TTR_ID3
		  when finalTestTable.TESTNAME4 = 'PTP_DOC400_RST_AND_BUZZER' then finalTestTable.TTR_ID4
		  when finalTestTable.TESTNAME5 = 'PTP_DOC400_RST_AND_BUZZER' then finalTestTable.TTR_ID5
		  when finalTestTable.TESTNAME6 = 'PTP_DOC400_RST_AND_BUZZER' then finalTestTable.TTR_ID6
		  when finalTestTable.TESTNAME7 = 'PTP_DOC400_RST_AND_BUZZER' then finalTestTable.TTR_ID7
		  when finalTestTable.TESTNAME8 = 'PTP_DOC400_RST_AND_BUZZER' then finalTestTable.TTR_ID8
		  when finalTestTable.TESTNAME9 = 'PTP_DOC400_RST_AND_BUZZER' then finalTestTable.TTR_ID9
	 else NULL end) as TTR_ID8,
	(case 
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME3 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME4 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME5 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME6 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME7 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME8 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME9 = 'PTP_DOC400_RDSN' then NULL
		  when finalTestTable.TESTNAME1 = 'PTP_DOC400_PRNT' then finalTestTable.TTR_ID
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_PRNT' then finalTestTable.TTR_ID2
		  when finalTestTable.TESTNAME3 = 'PTP_DOC400_PRNT' then finalTestTable.TTR_ID3
		  when finalTestTable.TESTNAME4 = 'PTP_DOC400_PRNT' then finalTestTable.TTR_ID4
		  when finalTestTable.TESTNAME5 = 'PTP_DOC400_PRNT' then finalTestTable.TTR_ID5
		  when finalTestTable.TESTNAME6 = 'PTP_DOC400_PRNT' then finalTestTable.TTR_ID6
		  when finalTestTable.TESTNAME7 = 'PTP_DOC400_PRNT' then finalTestTable.TTR_ID7
		  when finalTestTable.TESTNAME8 = 'PTP_DOC400_PRNT' then finalTestTable.TTR_ID8
		  when finalTestTable.TESTNAME9 = 'PTP_DOC400_PRNT' then finalTestTable.TTR_ID9
	 else NULL end) as TTR_ID9,   

	(case when finalTestTable.TESTNAME1 = 'PTP_DOC400_RDSN' then finalTestTable.TTR_VALUE else NULL end) as TTR_VALUE,
	(case 
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_RDSN' then NULL
		  when finalTestTable.TESTNAME1 = 'PTP_DOC400_QC' or finalTestTable.TESTNAME1 = 'PTP_DOC400_QC_WITHOUT_BURN' then finalTestTable.TTR_VALUE
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_QC' or finalTestTable.TESTNAME2 = 'PTP_DOC400_QC_WITHOUT_BURN' then finalTestTable.TTR_VALUE2
		  when finalTestTable.TESTNAME3 = 'PTP_DOC400_QC' or finalTestTable.TESTNAME3 = 'PTP_DOC400_QC_WITHOUT_BURN' then finalTestTable.TTR_VALUE3
		  when finalTestTable.TESTNAME4 = 'PTP_DOC400_QC' or finalTestTable.TESTNAME4 = 'PTP_DOC400_QC_WITHOUT_BURN' then finalTestTable.TTR_VALUE4
		  when finalTestTable.TESTNAME5 = 'PTP_DOC400_QC' or finalTestTable.TESTNAME5 = 'PTP_DOC400_QC_WITHOUT_BURN' then finalTestTable.TTR_VALUE5
		  when finalTestTable.TESTNAME6 = 'PTP_DOC400_QC' or finalTestTable.TESTNAME6 = 'PTP_DOC400_QC_WITHOUT_BURN' then finalTestTable.TTR_VALUE6
		  when finalTestTable.TESTNAME7 = 'PTP_DOC400_QC' or finalTestTable.TESTNAME7 = 'PTP_DOC400_QC_WITHOUT_BURN' then finalTestTable.TTR_VALUE7
		  when finalTestTable.TESTNAME8 = 'PTP_DOC400_QC' or finalTestTable.TESTNAME8 = 'PTP_DOC400_QC_WITHOUT_BURN' then finalTestTable.TTR_VALUE8
		  when finalTestTable.TESTNAME9 = 'PTP_DOC400_QC' or finalTestTable.TESTNAME9 = 'PTP_DOC400_QC_WITHOUT_BURN' then finalTestTable.TTR_VALUE9
	 else NULL end) as TTR_VALUE2,
	(case
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME3 = 'PTP_DOC400_RDSN' then NULL 
		  when finalTestTable.TESTNAME1 = 'PTP_DOC400_ZIGBEE' then finalTestTable.TTR_VALUE
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_ZIGBEE' then finalTestTable.TTR_VALUE2
		  when finalTestTable.TESTNAME3 = 'PTP_DOC400_ZIGBEE' then finalTestTable.TTR_VALUE3
		  when finalTestTable.TESTNAME4 = 'PTP_DOC400_ZIGBEE' then finalTestTable.TTR_VALUE4
		  when finalTestTable.TESTNAME5 = 'PTP_DOC400_ZIGBEE' then finalTestTable.TTR_VALUE5
		  when finalTestTable.TESTNAME6 = 'PTP_DOC400_ZIGBEE' then finalTestTable.TTR_VALUE6
		  when finalTestTable.TESTNAME7 = 'PTP_DOC400_ZIGBEE' then finalTestTable.TTR_VALUE7
		  when finalTestTable.TESTNAME8 = 'PTP_DOC400_ZIGBEE' then finalTestTable.TTR_VALUE8
		  when finalTestTable.TESTNAME9 = 'PTP_DOC400_ZIGBEE' then finalTestTable.TTR_VALUE9
	 else NULL end) as TTR_VALUE3,
	(case 
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME3 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME4 = 'PTP_DOC400_RDSN' then NULL
		  when finalTestTable.TESTNAME1 = 'PTP_DOC400_ZWAVE_PROG' then finalTestTable.TTR_VALUE
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_ZWAVE_PROG' then finalTestTable.TTR_VALUE2
		  when finalTestTable.TESTNAME3 = 'PTP_DOC400_ZWAVE_PROG' then finalTestTable.TTR_VALUE3
		  when finalTestTable.TESTNAME4 = 'PTP_DOC400_ZWAVE_PROG' then finalTestTable.TTR_VALUE4
		  when finalTestTable.TESTNAME5 = 'PTP_DOC400_ZWAVE_PROG' then finalTestTable.TTR_VALUE5
		  when finalTestTable.TESTNAME6 = 'PTP_DOC400_ZWAVE_PROG' then finalTestTable.TTR_VALUE6
		  when finalTestTable.TESTNAME7 = 'PTP_DOC400_ZWAVE_PROG' then finalTestTable.TTR_VALUE7
		  when finalTestTable.TESTNAME8 = 'PTP_DOC400_ZWAVE_PROG' then finalTestTable.TTR_VALUE8
		  when finalTestTable.TESTNAME9 = 'PTP_DOC400_ZWAVE_PROG' then finalTestTable.TTR_VALUE9
	 else NULL end) as TTR_VALUE4,
	(case
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME3 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME4 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME5 = 'PTP_DOC400_RDSN' then NULL 
		  when finalTestTable.TESTNAME1 = 'PTP_DOC400_ZWAVE_COMM' then finalTestTable.TTR_VALUE
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_ZWAVE_COMM' then finalTestTable.TTR_VALUE2
		  when finalTestTable.TESTNAME3 = 'PTP_DOC400_ZWAVE_COMM' then finalTestTable.TTR_VALUE3
		  when finalTestTable.TESTNAME4 = 'PTP_DOC400_ZWAVE_COMM' then finalTestTable.TTR_VALUE4
		  when finalTestTable.TESTNAME5 = 'PTP_DOC400_ZWAVE_COMM' then finalTestTable.TTR_VALUE5
		  when finalTestTable.TESTNAME6 = 'PTP_DOC400_ZWAVE_COMM' then finalTestTable.TTR_VALUE6
		  when finalTestTable.TESTNAME7 = 'PTP_DOC400_ZWAVE_COMM' then finalTestTable.TTR_VALUE7
		  when finalTestTable.TESTNAME8 = 'PTP_DOC400_ZWAVE_COMM' then finalTestTable.TTR_VALUE8
		  when finalTestTable.TESTNAME9 = 'PTP_DOC400_ZWAVE_COMM' then finalTestTable.TTR_VALUE9
	 else NULL end) as TTR_VALUE5,  
	(case
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME3 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME4 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME5 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME6 = 'PTP_DOC400_RDSN' then NULL 
		  when finalTestTable.TESTNAME1 = 'PTP_DOC400_WIFI' then finalTestTable.TTR_VALUE
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_WIFI' then finalTestTable.TTR_VALUE2
		  when finalTestTable.TESTNAME3 = 'PTP_DOC400_WIFI' then finalTestTable.TTR_VALUE3
		  when finalTestTable.TESTNAME4 = 'PTP_DOC400_WIFI' then finalTestTable.TTR_VALUE4
		  when finalTestTable.TESTNAME5 = 'PTP_DOC400_WIFI' then finalTestTable.TTR_VALUE5
		  when finalTestTable.TESTNAME6 = 'PTP_DOC400_WIFI' then finalTestTable.TTR_VALUE6
		  when finalTestTable.TESTNAME7 = 'PTP_DOC400_WIFI' then finalTestTable.TTR_VALUE7
		  when finalTestTable.TESTNAME8 = 'PTP_DOC400_WIFI' then finalTestTable.TTR_VALUE8
		  when finalTestTable.TESTNAME9 = 'PTP_DOC400_WIFI' then finalTestTable.TTR_VALUE9
	 else NULL end) as TTR_VALUE6, 
	(case 
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME3 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME4 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME5 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME6 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME7 = 'PTP_DOC400_RDSN' then NULL
		  when finalTestTable.TESTNAME1 = 'PTP_DOC400_FRONT_AND_LED' then finalTestTable.TTR_VALUE
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_FRONT_AND_LED' then finalTestTable.TTR_VALUE2
		  when finalTestTable.TESTNAME3 = 'PTP_DOC400_FRONT_AND_LED' then finalTestTable.TTR_VALUE3
		  when finalTestTable.TESTNAME4 = 'PTP_DOC400_FRONT_AND_LED' then finalTestTable.TTR_VALUE4
		  when finalTestTable.TESTNAME5 = 'PTP_DOC400_FRONT_AND_LED' then finalTestTable.TTR_VALUE5
		  when finalTestTable.TESTNAME6 = 'PTP_DOC400_FRONT_AND_LED' then finalTestTable.TTR_VALUE6
		  when finalTestTable.TESTNAME7 = 'PTP_DOC400_FRONT_AND_LED' then finalTestTable.TTR_VALUE7
		  when finalTestTable.TESTNAME8 = 'PTP_DOC400_FRONT_AND_LED' then finalTestTable.TTR_VALUE8
		  when finalTestTable.TESTNAME9 = 'PTP_DOC400_FRONT_AND_LED' then finalTestTable.TTR_VALUE9
	 else NULL end) as TTR_VALUE7,
	(case 
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME3 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME4 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME5 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME6 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME7 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME8 = 'PTP_DOC400_RDSN' then NULL
		  when finalTestTable.TESTNAME1 = 'PTP_DOC400_RST_AND_BUZZER' then finalTestTable.TTR_VALUE
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_RST_AND_BUZZER' then finalTestTable.TTR_VALUE2
		  when finalTestTable.TESTNAME3 = 'PTP_DOC400_RST_AND_BUZZER' then finalTestTable.TTR_VALUE3
		  when finalTestTable.TESTNAME4 = 'PTP_DOC400_RST_AND_BUZZER' then finalTestTable.TTR_VALUE4
		  when finalTestTable.TESTNAME5 = 'PTP_DOC400_RST_AND_BUZZER' then finalTestTable.TTR_VALUE5
		  when finalTestTable.TESTNAME6 = 'PTP_DOC400_RST_AND_BUZZER' then finalTestTable.TTR_VALUE6
		  when finalTestTable.TESTNAME7 = 'PTP_DOC400_RST_AND_BUZZER' then finalTestTable.TTR_VALUE7
		  when finalTestTable.TESTNAME8 = 'PTP_DOC400_RST_AND_BUZZER' then finalTestTable.TTR_VALUE8
		  when finalTestTable.TESTNAME9 = 'PTP_DOC400_RST_AND_BUZZER' then finalTestTable.TTR_VALUE9
	 else NULL end) as TTR_VALUE8,
	(case 
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME3 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME4 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME5 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME6 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME7 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME8 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME9 = 'PTP_DOC400_RDSN' then NULL
		  when finalTestTable.TESTNAME1 = 'PTP_DOC400_PRNT' then finalTestTable.TTR_VALUE
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_PRNT' then finalTestTable.TTR_VALUE2
		  when finalTestTable.TESTNAME3 = 'PTP_DOC400_PRNT' then finalTestTable.TTR_VALUE3
		  when finalTestTable.TESTNAME4 = 'PTP_DOC400_PRNT' then finalTestTable.TTR_VALUE4
		  when finalTestTable.TESTNAME5 = 'PTP_DOC400_PRNT' then finalTestTable.TTR_VALUE5
		  when finalTestTable.TESTNAME6 = 'PTP_DOC400_PRNT' then finalTestTable.TTR_VALUE6
		  when finalTestTable.TESTNAME7 = 'PTP_DOC400_PRNT' then finalTestTable.TTR_VALUE7
		  when finalTestTable.TESTNAME8 = 'PTP_DOC400_PRNT' then finalTestTable.TTR_VALUE8
		  when finalTestTable.TESTNAME9 = 'PTP_DOC400_PRNT' then finalTestTable.TTR_VALUE9
	 else NULL end) as TTR_VALUE9,

	(case when finalTestTable.TESTNAME1 = 'PTP_DOC400_RDSN' then finalTestTable.TEST1 else NULL end) as TEST1,
	(case 
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_RDSN' then NULL
		  when finalTestTable.TESTNAME1 = 'PTP_DOC400_QC' or finalTestTable.TESTNAME1 = 'PTP_DOC400_QC_WITHOUT_BURN' then finalTestTable.TEST1
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_QC' or finalTestTable.TESTNAME2 = 'PTP_DOC400_QC_WITHOUT_BURN' then finalTestTable.TEST2
		  when finalTestTable.TESTNAME3 = 'PTP_DOC400_QC' or finalTestTable.TESTNAME3 = 'PTP_DOC400_QC_WITHOUT_BURN' then finalTestTable.TEST3
		  when finalTestTable.TESTNAME4 = 'PTP_DOC400_QC' or finalTestTable.TESTNAME4 = 'PTP_DOC400_QC_WITHOUT_BURN' then finalTestTable.TEST4
		  when finalTestTable.TESTNAME5 = 'PTP_DOC400_QC' or finalTestTable.TESTNAME5 = 'PTP_DOC400_QC_WITHOUT_BURN' then finalTestTable.TEST5
		  when finalTestTable.TESTNAME6 = 'PTP_DOC400_QC' or finalTestTable.TESTNAME6 = 'PTP_DOC400_QC_WITHOUT_BURN' then finalTestTable.TEST6
		  when finalTestTable.TESTNAME7 = 'PTP_DOC400_QC' or finalTestTable.TESTNAME7 = 'PTP_DOC400_QC_WITHOUT_BURN' then finalTestTable.TEST7
		  when finalTestTable.TESTNAME8 = 'PTP_DOC400_QC' or finalTestTable.TESTNAME8 = 'PTP_DOC400_QC_WITHOUT_BURN' then finalTestTable.TEST8
		  when finalTestTable.TESTNAME9 = 'PTP_DOC400_QC' or finalTestTable.TESTNAME9 = 'PTP_DOC400_QC_WITHOUT_BURN' then finalTestTable.TEST9
	 else NULL end) as TEST2,
	(case 
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME3 = 'PTP_DOC400_RDSN' then NULL
		  when finalTestTable.TESTNAME1 = 'PTP_DOC400_ZIGBEE' then finalTestTable.TEST1
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_ZIGBEE' then finalTestTable.TEST2
		  when finalTestTable.TESTNAME3 = 'PTP_DOC400_ZIGBEE' then finalTestTable.TEST3
		  when finalTestTable.TESTNAME4 = 'PTP_DOC400_ZIGBEE' then finalTestTable.TEST4
		  when finalTestTable.TESTNAME5 = 'PTP_DOC400_ZIGBEE' then finalTestTable.TEST5
		  when finalTestTable.TESTNAME6 = 'PTP_DOC400_ZIGBEE' then finalTestTable.TEST6
		  when finalTestTable.TESTNAME7 = 'PTP_DOC400_ZIGBEE' then finalTestTable.TEST7
		  when finalTestTable.TESTNAME8 = 'PTP_DOC400_ZIGBEE' then finalTestTable.TEST8
		  when finalTestTable.TESTNAME9 = 'PTP_DOC400_ZIGBEE' then finalTestTable.TEST9
	 else NULL end) as TEST3,
	(case 
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME3 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME4 = 'PTP_DOC400_RDSN' then NULL
		  when finalTestTable.TESTNAME1 = 'PTP_DOC400_ZWAVE_PROG' then finalTestTable.TEST1
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_ZWAVE_PROG' then finalTestTable.TEST2
		  when finalTestTable.TESTNAME3 = 'PTP_DOC400_ZWAVE_PROG' then finalTestTable.TEST3
		  when finalTestTable.TESTNAME4 = 'PTP_DOC400_ZWAVE_PROG' then finalTestTable.TEST4
		  when finalTestTable.TESTNAME5 = 'PTP_DOC400_ZWAVE_PROG' then finalTestTable.TEST5
		  when finalTestTable.TESTNAME6 = 'PTP_DOC400_ZWAVE_PROG' then finalTestTable.TEST6
		  when finalTestTable.TESTNAME7 = 'PTP_DOC400_ZWAVE_PROG' then finalTestTable.TEST7
		  when finalTestTable.TESTNAME8 = 'PTP_DOC400_ZWAVE_PROG' then finalTestTable.TEST8
		  when finalTestTable.TESTNAME9 = 'PTP_DOC400_ZWAVE_PROG' then finalTestTable.TEST9
	 else NULL end) as TEST4,
	(case 
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME3 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME4 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME5 = 'PTP_DOC400_RDSN' then NULL
		  when finalTestTable.TESTNAME1 = 'PTP_DOC400_ZWAVE_COMM' then finalTestTable.TEST1
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_ZWAVE_COMM' then finalTestTable.TEST2
		  when finalTestTable.TESTNAME3 = 'PTP_DOC400_ZWAVE_COMM' then finalTestTable.TEST3
		  when finalTestTable.TESTNAME4 = 'PTP_DOC400_ZWAVE_COMM' then finalTestTable.TEST4
		  when finalTestTable.TESTNAME5 = 'PTP_DOC400_ZWAVE_COMM' then finalTestTable.TEST5
		  when finalTestTable.TESTNAME6 = 'PTP_DOC400_ZWAVE_COMM' then finalTestTable.TEST6
		  when finalTestTable.TESTNAME7 = 'PTP_DOC400_ZWAVE_COMM' then finalTestTable.TEST7
		  when finalTestTable.TESTNAME8 = 'PTP_DOC400_ZWAVE_COMM' then finalTestTable.TEST8
		  when finalTestTable.TESTNAME9 = 'PTP_DOC400_ZWAVE_COMM' then finalTestTable.TEST9
	else NULL end) as TEST5,
	(case 
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME3 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME4 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME5 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME6 = 'PTP_DOC400_RDSN' then NULL
		  when finalTestTable.TESTNAME1 = 'PTP_DOC400_WIFI' then finalTestTable.TEST1
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_WIFI' then finalTestTable.TEST2
		  when finalTestTable.TESTNAME3 = 'PTP_DOC400_WIFI' then finalTestTable.TEST3
		  when finalTestTable.TESTNAME4 = 'PTP_DOC400_WIFI' then finalTestTable.TEST4
		  when finalTestTable.TESTNAME5 = 'PTP_DOC400_WIFI' then finalTestTable.TEST5
		  when finalTestTable.TESTNAME6 = 'PTP_DOC400_WIFI' then finalTestTable.TEST6
		  when finalTestTable.TESTNAME7 = 'PTP_DOC400_WIFI' then finalTestTable.TEST7
		  when finalTestTable.TESTNAME8 = 'PTP_DOC400_WIFI' then finalTestTable.TEST8
		  when finalTestTable.TESTNAME9 = 'PTP_DOC400_WIFI' then finalTestTable.TEST9
	 else NULL end) as TEST6,
	(case 
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME3 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME4 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME5 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME6 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME7 = 'PTP_DOC400_RDSN' then NULL
		  when finalTestTable.TESTNAME1 = 'PTP_DOC400_FRONT_AND_LED' then finalTestTable.TEST1
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_FRONT_AND_LED' then finalTestTable.TEST2
		  when finalTestTable.TESTNAME3 = 'PTP_DOC400_FRONT_AND_LED' then finalTestTable.TEST3
		  when finalTestTable.TESTNAME4 = 'PTP_DOC400_FRONT_AND_LED' then finalTestTable.TEST4
		  when finalTestTable.TESTNAME5 = 'PTP_DOC400_FRONT_AND_LED' then finalTestTable.TEST5
		  when finalTestTable.TESTNAME6 = 'PTP_DOC400_FRONT_AND_LED' then finalTestTable.TEST6
		  when finalTestTable.TESTNAME7 = 'PTP_DOC400_FRONT_AND_LED' then finalTestTable.TEST7
		  when finalTestTable.TESTNAME8 = 'PTP_DOC400_FRONT_AND_LED' then finalTestTable.TEST8
		  when finalTestTable.TESTNAME9 = 'PTP_DOC400_FRONT_AND_LED' then finalTestTable.TEST9
	 else NULL end) as TEST7, 
	(case 
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME3 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME4 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME5 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME6 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME7 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME8 = 'PTP_DOC400_RDSN' then NULL
		  when finalTestTable.TESTNAME1 = 'PTP_DOC400_RST_AND_BUZZER' then finalTestTable.TEST1
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_RST_AND_BUZZER' then finalTestTable.TEST2
		  when finalTestTable.TESTNAME3 = 'PTP_DOC400_RST_AND_BUZZER' then finalTestTable.TEST3
		  when finalTestTable.TESTNAME4 = 'PTP_DOC400_RST_AND_BUZZER' then finalTestTable.TEST4
		  when finalTestTable.TESTNAME5 = 'PTP_DOC400_RST_AND_BUZZER' then finalTestTable.TEST5
		  when finalTestTable.TESTNAME6 = 'PTP_DOC400_RST_AND_BUZZER' then finalTestTable.TEST6
		  when finalTestTable.TESTNAME7 = 'PTP_DOC400_RST_AND_BUZZER' then finalTestTable.TEST7
		  when finalTestTable.TESTNAME8 = 'PTP_DOC400_RST_AND_BUZZER' then finalTestTable.TEST8
		  when finalTestTable.TESTNAME9 = 'PTP_DOC400_RST_AND_BUZZER' then finalTestTable.TEST9
	 else NULL end) as TEST8,
	(case 
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME3 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME4 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME5 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME6 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME7 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME8 = 'PTP_DOC400_RDSN' or finalTestTable.TESTNAME9 = 'PTP_DOC400_RDSN' then NULL
		  when finalTestTable.TESTNAME1 = 'PTP_DOC400_PRNT' then finalTestTable.TEST1
		  when finalTestTable.TESTNAME2 = 'PTP_DOC400_PRNT' then finalTestTable.TEST2
		  when finalTestTable.TESTNAME3 = 'PTP_DOC400_PRNT' then finalTestTable.TEST3
		  when finalTestTable.TESTNAME4 = 'PTP_DOC400_PRNT' then finalTestTable.TEST4
		  when finalTestTable.TESTNAME5 = 'PTP_DOC400_PRNT' then finalTestTable.TEST5
		  when finalTestTable.TESTNAME6 = 'PTP_DOC400_PRNT' then finalTestTable.TEST6
		  when finalTestTable.TESTNAME7 = 'PTP_DOC400_PRNT' then finalTestTable.TEST7
		  when finalTestTable.TESTNAME8 = 'PTP_DOC400_PRNT' then finalTestTable.TEST8
		  when finalTestTable.TESTNAME9 = 'PTP_DOC400_PRNT' then finalTestTable.TEST9
	 else NULL end) as TEST9
	from
	(select
		seventhCircle.*,
		tb9.TTR_ID9,
		i.MERGED_TTR_VALUES as TTR_VALUE9,
		tb9.TEST9,
		tb9.TESTNAME9
		from
			(select
			sixthCircle.*,
			tb8.TTR_ID8,
			h.MERGED_TTR_VALUES as TTR_VALUE8,
			tb8.TEST8,
			tb8.TESTNAME8
			from
				(select 
				fifthCircle.*,
				tb7.TTR_ID7,
				g.MERGED_TTR_VALUES as TTR_VALUE7,
				tb7.TEST7,
				tb7.TESTNAME7
				from
					(select
					fourthCircle.*,
					tb6.TTR_ID6,
					f.MERGED_TTR_VALUES as TTR_VALUE6,
					tb6.TEST6,
					tb6.TESTNAME6	
					from	
						(select
						thirdCircle.*,
						tb5.TTR_ID5,
						e.MERGED_TTR_VALUES as TTR_VALUE5,
						tb5.TEST5,
						tb5.TESTNAME5
						from
							(select
							secondCircle.*,
							tb4.TTR_ID4,
							d.MERGED_TTR_VALUES as TTR_VALUE4,
							tb4.TEST4,
							tb4.TESTNAME4
							from
								(select
								firstCircle.*,
								tb3.TTR_ID3,
								c.MERGED_TTR_VALUES as TTR_VALUE3,
								tb3.TEST3,
								tb3.TESTNAME3
								from
									(select
									tb1.TEXP_ID,
									tb1.TTR_ID,
									a.MERGED_TTR_VALUES as TTR_VALUE,
									tb1.TTR_EXECUTIONRESULT as TEST1,
									tb1.TTR_TESTNAME as TESTNAME1,
									tb2.TTR_ID2,
									b.MERGED_TTR_VALUES as TTR_VALUE2,
									tb2.TEST2,
									tb2.TESTNAME2
									from dbo.TTESTRESULTS as tb1
									outer apply 
									(select top 1
									dbo.TTESTRESULTS.TEXP_ID as TEXP_ID2,
									dbo.TTESTRESULTS.TTR_ID as TTR_ID2,
									dbo.TTESTRESULTS.TTR_EXECUTIONRESULT as TEST2,
									dbo.TTESTRESULTS.TTR_TESTNAME as TESTNAME2 from dbo.TTESTRESULTS where dbo.TTESTRESULTS.TTR_ID > tb1.TTR_ID and tb1.TEXP_ID = dbo.TTESTRESULTS.TEXP_ID) as tb2
									left join 
									dbo.tempMeasurementData as a on a.TTR_ID = tb1.TTR_ID 
									left join dbo.tempMeasurementData as b on b.TTR_ID = tb2.TTR_ID2
									where tb1.TTR_TESTNAME = 'PTP_DOC400_RDSN') as firstCircle
										  outer apply
										  (select top 1
										  dbo.TTESTRESULTS.TTR_ID as TTR_ID3,
										  dbo.TTESTRESULTS.TTR_EXECUTIONRESULT as TEST3,
										  dbo.TTESTRESULTS.TTR_TESTNAME as TESTNAME3 from dbo.TTESTRESULTS where dbo.TTESTRESULTS.TTR_ID > firstCircle.TTR_ID2 and firstCircle.TEXP_ID = dbo.TTESTRESULTS.TEXP_ID) as tb3
										  left join dbo.tempMeasurementData as c on c.TTR_ID = tb3.TTR_ID3
										  ) as secondCircle
											   outer apply
											   (select top 1
											   dbo.TTESTRESULTS.TTR_ID as TTR_ID4,
											   dbo.TTESTRESULTS.TTR_EXECUTIONRESULT as TEST4,
											   dbo.TTESTRESULTS.TTR_TESTNAME as TESTNAME4 from dbo.TTESTRESULTS where dbo.TTESTRESULTS.TTR_ID > secondCircle.TTR_ID3 and secondCircle.TEXP_ID = dbo.TTESTRESULTS.TEXP_ID) as tb4
											   left join dbo.tempMeasurementData as d on d.TTR_ID = tb4.TTR_ID4) as thirdCircle  
												   outer apply
												   (select top 1
												   dbo.TTESTRESULTS.TTR_ID as TTR_ID5,
												   dbo.TTESTRESULTS.TTR_EXECUTIONRESULT as TEST5,
												   dbo.TTESTRESULTS.TTR_TESTNAME as TESTNAME5 from dbo.TTESTRESULTS where dbo.TTESTRESULTS.TTR_ID > thirdCircle.TTR_ID4 and thirdCircle.TEXP_ID = dbo.TTESTRESULTS.TEXP_ID) as tb5
												   left join dbo.tempMeasurementData as e on e.TTR_ID = tb5.TTR_ID5) as fourthCircle 
														  outer apply
														  (select top 1
														  dbo.TTESTRESULTS.TTR_ID as TTR_ID6,
														  dbo.TTESTRESULTS.TTR_EXECUTIONRESULT as TEST6,
														  dbo.TTESTRESULTS.TTR_TESTNAME as TESTNAME6 from dbo.TTESTRESULTS where dbo.TTESTRESULTS.TTR_ID > fourthCircle.TTR_ID5 and fourthCircle.TEXP_ID = dbo.TTESTRESULTS.TEXP_ID) as tb6
														  left join dbo.tempMeasurementData as f on f.TTR_ID = tb6.TTR_ID6) as fifthCircle
																outer apply
															   (select top 1
															   dbo.TTESTRESULTS.TTR_ID as TTR_ID7,
															   dbo.TTESTRESULTS.TTR_EXECUTIONRESULT as TEST7,
															   dbo.TTESTRESULTS.TTR_TESTNAME as TESTNAME7 from dbo.TTESTRESULTS where dbo.TTESTRESULTS.TTR_ID > fifthCircle.TTR_ID6 and fifthCircle.TEXP_ID = dbo.TTESTRESULTS.TEXP_ID) as tb7
															   left join dbo.tempMeasurementData as g on g.TTR_ID = tb7.TTR_ID7) as sixthCircle
																	  outer apply
																	 (select top 1
																	 dbo.TTESTRESULTS.TTR_ID as TTR_ID8,
																	 dbo.TTESTRESULTS.TTR_EXECUTIONRESULT as TEST8,
																	 dbo.TTESTRESULTS.TTR_TESTNAME as TESTNAME8 from dbo.TTESTRESULTS where dbo.TTESTRESULTS.TTR_ID > sixthCircle.TTR_ID7 and sixthCircle.TEXP_ID = dbo.TTESTRESULTS.TEXP_ID) as tb8
																	 left join dbo.tempMeasurementData as h on h.TTR_ID = tb8.TTR_ID8) as seventhCircle
																		   outer apply
																		   (select top 1
																		   dbo.TTESTRESULTS.TTR_ID as TTR_ID9,
																		   dbo.TTESTRESULTS.TTR_EXECUTIONRESULT as TEST9,
																		   dbo.TTESTRESULTS.TTR_TESTNAME as TESTNAME9 from dbo.TTESTRESULTS where dbo.TTESTRESULTS.TTR_ID > seventhCircle.TTR_ID8 and seventhCircle.TEXP_ID = dbo.TTESTRESULTS.TEXP_ID) as tb9
																		   left join dbo.tempMeasurementData as i on i.TTR_ID = tb9.TTR_ID9) as finalTestTable) as finalBugFixTable) as finalTableWithoutUsers
																		   
					left join dbo.TEXECUTIONPARAMETERS on finalTableWithoutUsers.TEXP_ID = dbo.TEXECUTIONPARAMETERS.TEXP_ID
					left join dbo.TUSER on dbo.TEXECUTIONPARAMETERS.TU_ID = dbo.TUSER.TU_ID
					WHERE DATEDIFF(ss, @DateFrom, dbo.TEXECUTIONPARAMETERS.TEXP_START_DATETIME) >= 0 and
					DATEDIFF(ss, dbo.TEXECUTIONPARAMETERS.TEXP_START_DATETIME, @DateTo) >= 0 
	
	SET NOCOUNT ON;
END
